﻿using Perspective.Wpf3D.Shapes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Wpf3DSample
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            axis3DViewport.Children.Add(new XyzAxis3D() { Length = 4, Radius = 0.03 });
            //var triangle = CreateTriangleModel(new Point3D(0, 1, 0), new Point3D(0, 0, 1), new Point3D(1, 0, 0), Brushes.Red);
            //var plane = CreatePlane(new Point3D(0, 1, 1), new Point3D(0, 0, 1), new Point3D(1, 0, 0), new Point3D(1, 1, 0), Brushes.Blue);
            //var cube = CreateCubeModel(new Point3D(0, 0, 0), 2, 1, 3, Brushes.Yellow);
            var sphere = CreateSphereModel(new Point3D(0, 0, 0), Brushes.Purple, 2);
            axis3DViewport.Children.Add(sphere);
        }

        public static Spherical3D CreateSphereModel(Point3D centerPoint, SolidColorBrush brush = null, double radius = 1)
        {
            var sphere = new Spherical3D() { Material = new DiffuseMaterial(brush != null ? brush : Brushes.Red) };
            sphere = TranslateSphere(sphere, centerPoint, radius);
            return sphere;
        }

        // Translate sphere to specified position and size
        public static Spherical3D TranslateSphere(Spherical3D sphere, Point3D centerPoint, double radius = 1)
        {
            var tgroup = sphere.Transform as Transform3DGroup;
            if (tgroup == null)
            {
                tgroup = new Transform3DGroup();
                tgroup.Children.Add(new ScaleTransform3D(radius, radius, radius));
                var tt3D = new TranslateTransform3D(centerPoint.X, centerPoint.Y, centerPoint.Z);
                tgroup.Children.Add(tt3D);
            }
            else
            {
                var tt3D = tgroup.Children[1] as TranslateTransform3D;
                tt3D.OffsetX = centerPoint.X;
                tt3D.OffsetY = centerPoint.Y;
                tt3D.OffsetZ = centerPoint.Z;
                tgroup.Children[1] = tt3D;
            }
            sphere.Transform = tgroup;
            return sphere;
        }

        // width -> x axis length, height -> y axis length, depth -> z axis length,
        public static Model3DGroup CreateCubeModel(Point3D centerPoint, double width, double height, double depth, SolidColorBrush brush = null)
        {
            var cube = new Model3DGroup();
            var p0 = new Point3D(centerPoint.X - width / 2, centerPoint.Y - height / 2, centerPoint.Z - depth / 2);
            var p1 = new Point3D(p0.X + width, p0.Y, p0.Z);
            var p2 = new Point3D(p0.X + width, p0.Y, p0.Z + depth);
            var p3 = new Point3D(p0.X, p0.Y, p0.Z + depth);
            var p4 = new Point3D(p0.X, p0.Y + height, p0.Z);
            var p5 = new Point3D(p0.X + width, p0.Y + height, p0.Z);
            var p6 = new Point3D(p0.X + width, p0.Y + height, p0.Z + depth);
            var p7 = new Point3D(p0.X, p0.Y + height, p0.Z + depth);
            //front side triangles
            cube.Children.Add(CreateTriangleModel(p3, p2, p6, brush));
            cube.Children.Add(CreateTriangleModel(p3, p6, p7, brush));
            //right side triangles
            cube.Children.Add(CreateTriangleModel(p2, p1, p5, brush));
            cube.Children.Add(CreateTriangleModel(p2, p5, p6, brush));
            //back side triangles
            cube.Children.Add(CreateTriangleModel(p1, p0, p4, brush));
            cube.Children.Add(CreateTriangleModel(p1, p4, p5, brush));
            //left side triangles
            cube.Children.Add(CreateTriangleModel(p0, p3, p7, brush));
            cube.Children.Add(CreateTriangleModel(p0, p7, p4, brush));
            //top side triangles
            cube.Children.Add(CreateTriangleModel(p7, p6, p5, brush));
            cube.Children.Add(CreateTriangleModel(p7, p5, p4, brush));
            //bottom side triangles
            cube.Children.Add(CreateTriangleModel(p2, p3, p0, brush));
            cube.Children.Add(CreateTriangleModel(p2, p0, p1, brush));
            return cube;
        }

        // p0 -> p1 -> p2 -> p3 に対し、右ねじの方向に四角形を表示する
        public static Model3DGroup CreatePlane(Point3D p0, Point3D p1, Point3D p2, Point3D p3, SolidColorBrush brush = null)
        {
            var plane = new Model3DGroup();
            plane.Children.Add(CreateTriangleModel(p0, p1, p3, brush));
            plane.Children.Add(CreateTriangleModel(p2, p3, p1, brush));
            return plane;
        }

        // 右ねじの方向が表、裏には何も表示されない
        public static Model3DGroup CreateTriangleModel(Point3D p0, Point3D p1, Point3D p2, SolidColorBrush brush = null)
        {
            var pointsList = new List<Point3D>() { p0, p1, p2 };
            var triangleMesh = new MeshGeometry3D();
            // Set each edge positions of triangle
            triangleMesh.Positions = new Point3DCollection(pointsList);
            var normal = CalculateNormals(p0, p1, p2);
            triangleMesh.Normals = new Vector3DCollection(new List<Vector3D>() { normal, normal, normal });
            var material = new DiffuseMaterial(brush != null ? brush : Brushes.White);
            var model = new GeometryModel3D(triangleMesh, material);
            var model3Dgroup = new Model3DGroup();
            model3Dgroup.Children.Add(model);
            return model3Dgroup;
        }

        // 外積の計算
        public static Vector3D CalculateNormals(Point3D p0, Point3D p1, Point3D p2)
        {
            return Vector3D.CrossProduct(p1 - p0, p2 - p1);
        }
    }
}
