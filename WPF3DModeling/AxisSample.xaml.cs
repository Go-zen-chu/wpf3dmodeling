﻿using Perspective.Wpf3D.Shapes;
using System.Windows.Controls;

namespace WPF3DModeling
{
    /// <summary>
    /// AxisSample.xaml の相互作用ロジック
    /// </summary>
    public partial class AxisSample : UserControl
    {
        public AxisSample()
        {
            InitializeComponent();
            axis3DViewport.Children.Add(new XyzAxis3D() { Length = 10, Radius = 0.03 });
        }
    }
}
