﻿using System;
using System.Windows;

namespace WPF3DModeling
{
	class MainClass
	{
		public static void Main (string[] args)
		{
            var w = new Window() { Content = new AxisSample(), Height = 300, Width = 300 };
            w.Show();
		}
	}
}
