﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Perspective.Wpf3D.Shapes;
using Perspective.Core.Wpf;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using System.Linq;
using System.Windows.Media.Imaging;
using System.IO;

namespace WPF3DModeling
{
    public partial class WPF3DModelingControl : UserControl
    {
        DirectionalLight directionalLight;
        Point? dragStartPoint;

        List<Spherical3D> sphereList;
        List<Bar3D> cylinderList;
        Dictionary<Bar3D, Tuple<Spherical3D, Spherical3D>> connectedElements = new Dictionary<Bar3D, Tuple<Spherical3D, Spherical3D>>();
        Storyboard animationStoryBoard;
        DispatcherTimer cylinderAnimationTimer;
        // 振動の向きを指し示す矢印
        List<ModelVisual3D> arrowList = new List<ModelVisual3D>();

        TextBlock peakValueTextBlock;

        static Func<Point3D, Point3D, Point3D> getCenterPoint = (p1, p2) => new Point3D((p1.X + p2.X) / 2, (p1.Y + p2.Y) / 2, (p1.Z + p2.Z) / 2);

        /*
        public WPF3DModelingControl()
        {
            InitializeComponent();
            var cameraMV3D = new ModelVisual3D();
            directionalLight = new DirectionalLight(Colors.White, new Vector3D(0, 0, -1));
            cameraMV3D.Content = directionalLight;
            modeling3DViewport.Children.Add(cameraMV3D);
#if DEBUG
            modeling3DViewport.Children.Add(new XyzAxis3D() { Length = 10, Radius = 0.03 });
#endif
        }


        public void DrawCompound()
        {
            #region check

            if (compoundInfo == null) throw new ArgumentNullException("Compound info has not been set. Check the Property Value!");

            #endregion
            if (sphereList != null) sphereList.ForEach(s => compoundViewport3D.Children.Remove(s));
            if (cylinderList != null) cylinderList.ForEach(c => compoundViewport3D.Children.Remove(c));
            sphereList = new List<Spherical3D>();
            cylinderList = new List<Bar3D>();
            for (int elemIdx = 0; elemIdx < CompoundInfo.struct_info.elements.Count; elemIdx++)
            {
                var elem = CompoundInfo.struct_info.elements[elemIdx];
                var elemColor = ColorConverter.ConvertFromString(ElementValue.ElementColor[elem.name]) as Color?;
                var elemRadius = ElementValue.GetElementSize(elem.name);
                if (elemColor != null)
                    sphereList.Add(CreateSphere(new Point3D(), brush: new SolidColorBrush(elemColor.Value), radius: elemRadius));
            }
            foreach (var bond in CompoundInfo.struct_info.bonds)
            {
                var bondColor = ColorConverter.ConvertFromString(ElementValue.BondColor[bond.type]) as Color?;
                if (bondColor != null)
                    cylinderList.Add(CreateCylinder(new Point3D(), new Point3D(), brush: new SolidColorBrush(bondColor.Value), radius: 0.15));
            }
            MoveCompoundToDefault();

            cylinderList.ForEach(c => compoundViewport3D.Children.Add(c));
            sphereList.ForEach(s => compoundViewport3D.Children.Add(s));

            // どの円筒にどの球がつながっているかを記録
            if (connectedElements != null) connectedElements.Clear();
            foreach (var tpl in CompoundInfo.struct_info.bonds.Select((b, idx) => new { V = b, I = idx }))
                connectedElements.Add(cylinderList[tpl.I], new Tuple<Spherical3D, Spherical3D>(sphereList[tpl.V.terms[0]], sphereList[tpl.V.terms[1]]));
        }

        public void MoveCompoundToDefault()
        {
            #region check
            if (compoundInfo == null) throw new ArgumentNullException("Compound info has not been set. Check the Property Value!");
            if (sphereList == null && cylinderList == null) return;

            #endregion
            var elementPosDict = new Dictionary<int, Point3D>();
            for (var elemIdx = 0; elemIdx < CompoundInfo.struct_info.elements.Count; elemIdx++)
            {
                var elem = CompoundInfo.struct_info.elements[elemIdx];
                var centerPoint = new Point3D(elem.pos[0], elem.pos[1], elem.pos[2]);
                elementPosDict.Add(elemIdx, centerPoint);
                sphereList[elemIdx] = MoveSphere(sphereList[elemIdx], centerPoint);
            }
            for (int bondIdx = 0; bondIdx < CompoundInfo.struct_info.bonds.Count; bondIdx++)
            {
                var bond = CompoundInfo.struct_info.bonds[bondIdx];
                cylinderList[bondIdx] = MoveCylinder(cylinderList[bondIdx], elementPosDict[bond.terms[0]], elementPosDict[bond.terms[1]]);
            }
        }

        public void StartCompoundAnimation(double peakValue)
        {
            #region check
            if (compoundInfo == null) throw new ArgumentNullException("Compound info has not been set. Check the Property Value!");
            if (compoundVibration == null) throw new ArgumentNullException("CompoundVibration is not set. Check the Property Value!");
            if (sphereList == null || cylinderList == null) return;
            // ある波数における振動データ
            if (peakVibrationDict.ContainsKey(peakValue) == false)
                throw new KeyNotFoundException("Could not find peak value in peakVibrationDict! Make sure your peak value is valid");

            #endregion
            animationStoryBoard = new Storyboard();
            var vibDatum = peakVibrationDict[peakValue];
            for (int elemIdx = 0; elemIdx < sphereList.Count; elemIdx++)
            {
                #region 原子のアニメーション
                var sphere = sphereList[elemIdx];
                var elemPos = CompoundInfo.struct_info.elements[elemIdx].pos;
                var tt3D = (sphere.Transform as Transform3DGroup).Children[1] as TranslateTransform3D;
                // アニメーションのための名前登録
                var ttname = "TranslateTransform3D_" + tt3D.GetHashCode();
                RegisterName(ttname, tt3D);

                var xAmplitude = vibDatum.vibration_x[elemIdx];
                var xAnimation = new DoubleAnimation(elemPos[0] + xAmplitude, elemPos[0] - xAmplitude, new Duration(TimeSpan.FromSeconds(1)))
                {
                    AutoReverse = true,
                    RepeatBehavior = RepeatBehavior.Forever
                };
                Storyboard.SetTargetName(xAnimation, ttname);
                Storyboard.SetTargetProperty(xAnimation, new PropertyPath(TranslateTransform3D.OffsetXProperty));

                var yAmplitude = vibDatum.vibration_y[elemIdx];
                var yAnimation = new DoubleAnimation(elemPos[1] + yAmplitude, elemPos[1] - yAmplitude, new Duration(TimeSpan.FromSeconds(1)))
                {
                    AutoReverse = true,
                    RepeatBehavior = RepeatBehavior.Forever
                };
                Storyboard.SetTargetName(yAnimation, ttname);
                Storyboard.SetTargetProperty(yAnimation, new PropertyPath(TranslateTransform3D.OffsetYProperty));

                var zAmplitude = vibDatum.vibration_z[elemIdx];
                var zAnimation = new DoubleAnimation(elemPos[2] + zAmplitude, elemPos[2] - zAmplitude, new Duration(TimeSpan.FromSeconds(1)))
                {
                    AutoReverse = true,
                    RepeatBehavior = RepeatBehavior.Forever
                };
                Storyboard.SetTargetName(zAnimation, ttname);
                Storyboard.SetTargetProperty(zAnimation, new PropertyPath(TranslateTransform3D.OffsetZProperty));

                animationStoryBoard.Children.Add(xAnimation);
                animationStoryBoard.Children.Add(yAnimation);
                animationStoryBoard.Children.Add(zAnimation);
                #endregion
            }
            // isControllableをtrueにしなければ止められない
            animationStoryBoard.Begin(this, true);

            // 50ms ごとに原子の座標にアクセスして、円筒の座標を変更する
            cylinderAnimationTimer = new DispatcherTimer() { Interval = new TimeSpan(0, 0, 0, 0, 50) };
            cylinderAnimationTimer.Tick += (object sender, EventArgs e) =>
            {
                #region 結合のアニメーション
                cylinderList.ForEach(cylinder =>
                {
                    var elem1 = connectedElements[cylinder].Item1;
                    var elem1tt3D = (elem1.Transform as Transform3DGroup).Children[1] as TranslateTransform3D;
                    var elem1Pos = new Point3D(elem1tt3D.OffsetX, elem1tt3D.OffsetY, elem1tt3D.OffsetZ);

                    var elem2 = connectedElements[cylinder].Item2;
                    var elem2tt3D = (elem2.Transform as Transform3DGroup).Children[1] as TranslateTransform3D;
                    var elem2Pos = new Point3D(elem2tt3D.OffsetX, elem2tt3D.OffsetY, elem2tt3D.OffsetZ);

                    cylinder = MoveCylinder(cylinder, elem1Pos, elem2Pos);
                });
                #endregion
            };
            cylinderAnimationTimer.Start();

            // peakの値を表示する
            if (peakValueTextBlock == null)
            {
                peakValueTextBlock = new TextBlock()
                {
                    Background = Brushes.Black,
                    Opacity = 0.7,
                    Foreground = Brushes.White,
                    FontSize = 16,
                    HorizontalAlignment = System.Windows.HorizontalAlignment.Left,
                    VerticalAlignment = System.Windows.VerticalAlignment.Top,
                    Margin = new Thickness(10, 10, 0, 0)
                };
                compound3dControlGrid.Children.Add(peakValueTextBlock);
            }
            peakValueTextBlock.Visibility = System.Windows.Visibility.Visible;
            peakValueTextBlock.Text = peakValue + " cm-1";
        }

        public void StopCompoundAnimation()
        {
            #region check
            if (compoundInfo == null) throw new Exception("Compound info has not been set. Check the Property Value!");
            if (compoundVibration == null) throw new Exception("CompoundVibration is not set. Check the Property Value!");
            if (sphereList == null || cylinderList == null) return;

            #endregion
            if (animationStoryBoard != null) animationStoryBoard.Stop(this);
            if (cylinderAnimationTimer != null) cylinderAnimationTimer.Stop();
            sphereList.ForEach(sphere =>
            {
                var tt3D = (sphere.Transform as Transform3DGroup).Children[1] as TranslateTransform3D;
                var ttname = "TranslateTransform3D_" + tt3D.GetHashCode();
                UnregisterName(ttname);
            });
            MoveCompoundToDefault();
            if (peakValueTextBlock == null) return;
            peakValueTextBlock.Visibility = System.Windows.Visibility.Collapsed;
        }

        public void ShowVectors(double peakValue)
        {
            #region check
            if (compoundInfo == null) throw new Exception("Compound info has not been set. Check the Property Value!");
            if (compoundVibration == null) throw new Exception("CompoundVibration is not set. Check the Property Value!");
            // ある波数における振動データ
            if (peakVibrationDict.ContainsKey(peakValue) == false)
                throw new KeyNotFoundException("Could not find peak value in peakVibrationDict! Make sure your peak value is valid");
            if (sphereList == null) return;
            #endregion
            var vibDatum = peakVibrationDict[peakValue];
            // 振動の向きを表示
            HideVectors();
            for (int elemIdx = 0; elemIdx < sphereList.Count; elemIdx++)
            {
                var elemDefaultPos = CompoundInfo.struct_info.elements[elemIdx].pos;
                var vibrateEndPoint1 = new Point3D(elemDefaultPos[0] + vibDatum.vibration_x[elemIdx] * 2,
                    elemDefaultPos[1] + vibDatum.vibration_y[elemIdx] * 2,
                    elemDefaultPos[2] + vibDatum.vibration_z[elemIdx] * 2);
                var vibrateEndPoint2 = new Point3D(elemDefaultPos[0] - vibDatum.vibration_x[elemIdx] * 2,
                    elemDefaultPos[1] - vibDatum.vibration_y[elemIdx] * 2,
                    elemDefaultPos[2] - vibDatum.vibration_z[elemIdx] * 2);
                var vibrateArrow = CreateBothWayArrow(vibrateEndPoint1, vibrateEndPoint2, 0.6, 0.3,
                    brush: new SolidColorBrush() { Color = Colors.Blue, Opacity = 0.6 });
                var arrowModel = new ModelVisual3D() { Content = vibrateArrow };
                arrowList.Add(arrowModel);
                compoundViewport3D.Children.Add(arrowModel);
            }
        }

        public void HideVectors()
        {
            if (arrowList != null && arrowList.Count > 0)
            {
                arrowList.ForEach(a => compoundViewport3D.Children.Remove(a));
                arrowList.Clear();
            }
        }

        public static Spherical3D CreateSphere(Point3D centerPoint, SolidColorBrush brush = null, double radius = 1)
        {
            var sphere = new Spherical3D() { Material = new DiffuseMaterial(brush != null ? brush : Brushes.Red) };
            sphere = MoveSphere(sphere, centerPoint, radius);
            return sphere;
        }

        public static Spherical3D MoveSphere(Spherical3D sphere, Point3D centerPoint, double radius = 1)
        {
            #region check
            if (sphere == null) throw new ArgumentNullException();

            #endregion
            var tgroup = sphere.Transform as Transform3DGroup;
            if (tgroup == null)
            {
                tgroup = new Transform3DGroup();
                tgroup.Children.Add(new ScaleTransform3D(radius, radius, radius));
                var tt3D = new TranslateTransform3D(centerPoint.X, centerPoint.Y, centerPoint.Z);
                tgroup.Children.Add(tt3D);
            }
            else
            {
                var tt3D = tgroup.Children[1] as TranslateTransform3D;
                tt3D.OffsetX = centerPoint.X;
                tt3D.OffsetY = centerPoint.Y;
                tt3D.OffsetZ = centerPoint.Z;
                tgroup.Children[1] = tt3D;
            }

            sphere.Transform = tgroup;
            return sphere;
        }

        public static Bar3D CreateCylinder(Point3D p0, Point3D p1, SolidColorBrush brush = null, double radius = 0.15, int resolution = 100)
        {
            #region check
            if ((p0 - p1).Length == 0) throw new ArgumentException("length between p0 and p1 should be more than 0");
            if (resolution > 2000) throw new ArgumentOutOfRangeException("resolution should be under 2000");

            #endregion
            var cylinder = new Bar3D()
            {
                Material = new DiffuseMaterial(brush != null ? brush : Brushes.Silver),
                SideCount = resolution
            };
            cylinder = MoveCylinder(cylinder, p0, p1, radius);
            return cylinder;
        }

        // 初期座標から円筒の位置を計算し直す
        public static Bar3D MoveCylinder(Bar3D cylinder, Point3D p0, Point3D p1, double radius = 0.15)
        {
            #region check
            if (cylinder == null) throw new ArgumentNullException();
            if ((p0 - p1).Length == 0) throw new ArgumentException("length between p0 and p1 should be larger than 0");

            #endregion
            var zAxisVector = new Vector3D(0, 0, 1);
            // 回転後の円筒の軸となるベクトル
            // *この方法では
            var newAxisVector = p1 - p0;
            var normalizedNewAxis = newAxisVector;
            normalizedNewAxis.Normalize();
            // デフォルトの円筒の軸であるz軸と、回転後の軸の垂直ベクトルを求める
            var perpendicular = Vector3D.CrossProduct(zAxisVector, normalizedNewAxis);
            // デフォルトの円筒の軸であるz軸と、回転後の軸の回転角を求め、内積とarccosで角度を求める
            var rotateAngle = Math.Acos(Vector3D.DotProduct(zAxisVector, normalizedNewAxis)) * 180 / Math.PI;
            // 結合の中心点
            var bondCenter = getCenterPoint(p0, p1);

            var tgroup = cylinder.Transform as Transform3DGroup;
            if (tgroup == null)
            {
                tgroup = new Transform3DGroup();
                // 円筒は初期値で(0,0,0)->(0,0,1)となっているため、(0,0,-0.5)->(0,0,0.5)に移動（底面1の中心 -> 底面2の中心）
                tgroup.Children.Add(new TranslateTransform3D(0, 0, -0.5));
                // 長さの変更
                tgroup.Children.Add(new ScaleTransform3D(radius, radius, newAxisVector.Length));
                // 角度の変更
                tgroup.Children.Add(new RotateTransform3D(new AxisAngleRotation3D(perpendicular, rotateAngle)));
                // 中心の移動
                tgroup.Children.Add(new TranslateTransform3D(bondCenter.X, bondCenter.Y, bondCenter.Z));
            }
            else
            {
                // 長さの変更
                var st3D = tgroup.Children[1] as ScaleTransform3D;
                st3D.ScaleZ = newAxisVector.Length;
                tgroup.Children[1] = st3D;
                // 角度の変更
                var rt3D = tgroup.Children[2] as RotateTransform3D;
                rt3D.Rotation = new AxisAngleRotation3D(perpendicular, rotateAngle);
                tgroup.Children[2] = rt3D;
                // 中心の移動
                var tt3D = tgroup.Children[3] as TranslateTransform3D;
                tt3D.OffsetX = bondCenter.X; tt3D.OffsetY = bondCenter.Y; tt3D.OffsetZ = bondCenter.Z;
                tgroup.Children[3] = tt3D;
            }
            cylinder.Transform = tgroup;
            return cylinder;
        }

        public Model3DGroup CreateAxis(double axisLength = 100, double axisThickness = 0.5)
        {
            var axises = new Model3DGroup();
            var xAxis = CreateCubeModel(new Point3D(axisLength / 2, axisThickness / 2, axisThickness / 2), axisLength, axisThickness, axisThickness, Brushes.Red);
            var yAxis = CreateCubeModel(new Point3D(axisThickness / 2, axisLength / 2, axisThickness / 2), axisThickness, axisLength, axisThickness, Brushes.Blue);
            var zAxis = CreateCubeModel(new Point3D(axisThickness / 2, axisThickness / 2, axisLength / 2), axisThickness, axisThickness, axisLength, Brushes.Green);
            axises.Children = new Model3DCollection(new List<Model3DGroup>() { xAxis, yAxis, zAxis });
            return axises;
        }

        // headLengthRateは矢印の頭の長さの割合
        public static Model3DGroup CreateBothWayArrow(Point3D fromPoint, Point3D toPoint, double width, double height, double headLengthRate = 0.3, SolidColorBrush brush = null)
        {
            #region check
            if ((fromPoint - toPoint).Length == 0) throw new ArgumentException("arrow length should be larger than 0");
            if (width <= 0 || height <= 0) throw new ArgumentException("width and height should be > 0");
            if (headLengthRate <= 0 || 1 <= headLengthRate) throw new ArgumentException("headLengthRate has to be more than 0, less than 1");

            #endregion
            var bwa = new Model3DGroup();
            var centerPoint = getCenterPoint(fromPoint, toPoint);
            var arrow1 = CreateSingleWayArrow(centerPoint, fromPoint, width, height, headLengthRate, brush);
            var arrow2 = CreateSingleWayArrow(centerPoint, toPoint, width, height, headLengthRate, brush);
            bwa.Children = new Model3DCollection(new List<Model3DGroup>() { arrow1, arrow2 });
            return bwa;
        }

        public static Model3DGroup CreateSingleWayArrow(Point3D fromPoint, Point3D toPoint, double width, double height, double headLengthRate = 0.3, SolidColorBrush brush = null)
        {
            #region check
            if ((fromPoint - toPoint).Length == 0) throw new ArgumentException("arrow length should be larger than 0");
            if (width <= 0 || height <= 0) throw new ArgumentException("width and height should be > 0");
            if (headLengthRate <= 0 || 1 <= headLengthRate) throw new Exception("headLengthRate has to be more than 0, less than 1");

            #endregion

            var swa = new Model3DGroup();
            var heightVector = new Vector3D(0, height, 0);

            // p0が先端
            var p0 = new Point3D(0, height / 2, 0.5);
            var headWidth = width * (1 + headLengthRate * 2);
            var p1 = new Point3D(-headWidth / 2, height / 2, (0.5 - headLengthRate));
            var p2 = new Point3D(headWidth / 2, height / 2, (0.5 - headLengthRate));
            // p5,p6は矢印の元
            var p3 = new Point3D(-width / 2, height / 2, (0.5 - headLengthRate));
            var p4 = new Point3D(width / 2, height / 2, (0.5 - headLengthRate));
            var p5 = new Point3D(width / 2, height / 2, -0.5);
            var p6 = new Point3D(-width / 2, height / 2, -0.5);
            // 先端の描画
            // 三角の描画
            swa.Children.Add(CreateTriangleModel(p0, p2, p1, brush));
            swa.Children.Add(CreateTriangleModel(p0 - heightVector, p1 - heightVector, p2 - heightVector, brush));
            // 先端の側面の描画
            swa.Children.Add(CreatePlane(p0, p1, p1 - heightVector, p0 - heightVector, brush));
            swa.Children.Add(CreatePlane(p2, p0, p0 - heightVector, p2 - heightVector, brush));
            swa.Children.Add(CreatePlane(p4, p2, p2 - heightVector, p4 - heightVector, brush));
            swa.Children.Add(CreatePlane(p1, p3, p3 - heightVector, p1 - heightVector, brush));

            // 先端以外の描画
            swa.Children.Add(CreatePlane(p3, p4, p5, p6, brush));
            swa.Children.Add(CreatePlane(p3 - heightVector, p6 - heightVector, p5 - heightVector, p4 - heightVector, brush));
            //側面の描画
            swa.Children.Add(CreatePlane(p5, p4, p4 - heightVector, p5 - heightVector, brush));
            swa.Children.Add(CreatePlane(p6, p5, p5 - heightVector, p6 - heightVector, brush));
            swa.Children.Add(CreatePlane(p3, p6, p6 - heightVector, p3 - heightVector, brush));

            swa = TransformModel3D(swa, fromPoint, toPoint, width, height) as Model3DGroup;

            return swa;
        }

        // width -> x axis length, height -> y axis length, depth -> z axis length,
        public static Model3DGroup CreateCubeModel(Point3D centerPoint, double width, double height, double depth, SolidColorBrush brush = null)
        {
            var cube = new Model3DGroup();
            var p0 = new Point3D(centerPoint.X - width / 2, centerPoint.Y - height / 2, centerPoint.Z - depth / 2);
            var p1 = new Point3D(p0.X + width, p0.Y, p0.Z);
            var p2 = new Point3D(p0.X + width, p0.Y, p0.Z + depth);
            var p3 = new Point3D(p0.X, p0.Y, p0.Z + depth);
            var p4 = new Point3D(p0.X, p0.Y + height, p0.Z);
            var p5 = new Point3D(p0.X + width, p0.Y + height, p0.Z);
            var p6 = new Point3D(p0.X + width, p0.Y + height, p0.Z + depth);
            var p7 = new Point3D(p0.X, p0.Y + height, p0.Z + depth);
            //front side triangles
            cube.Children.Add(CreateTriangleModel(p3, p2, p6, brush));
            cube.Children.Add(CreateTriangleModel(p3, p6, p7, brush));
            //right side triangles
            cube.Children.Add(CreateTriangleModel(p2, p1, p5, brush));
            cube.Children.Add(CreateTriangleModel(p2, p5, p6, brush));
            //back side triangles
            cube.Children.Add(CreateTriangleModel(p1, p0, p4, brush));
            cube.Children.Add(CreateTriangleModel(p1, p4, p5, brush));
            //left side triangles
            cube.Children.Add(CreateTriangleModel(p0, p3, p7, brush));
            cube.Children.Add(CreateTriangleModel(p0, p7, p4, brush));
            //top side triangles
            cube.Children.Add(CreateTriangleModel(p7, p6, p5, brush));
            cube.Children.Add(CreateTriangleModel(p7, p5, p4, brush));
            //bottom side triangles
            cube.Children.Add(CreateTriangleModel(p2, p3, p0, brush));
            cube.Children.Add(CreateTriangleModel(p2, p0, p1, brush));
            return cube;
        }

        // z軸正方向のベクトルが p0=>p1のベクトルに変換されるようなTransformを行う(xScale, yScaleで幅や厚さなどを決める)
        public static Model3D TransformModel3D(Model3D model3D, Point3D p0, Point3D p1, double xScale = 1, double yScale = 1)
        {
            #region check
            if (model3D == null) throw new ArgumentNullException("model3D is null");
            if ((p0 - p1).Length == 0) throw new ArgumentException("Length between p0 and p1 is 0");
            if (xScale <= 0 || yScale <= 0) throw new ArgumentException("xScale and yScale have to be larger than 0");
            #endregion

            var yVector = new Vector3D(0, 1, 0);
            // 回転後の軸となるベクトル
            var newAxisVector = p1 - p0;
            // x,zの回転角
            var theta = (newAxisVector.Z == 0) ?
                (newAxisVector.X > 0 ? 90 : 180) :
                Math.Atan(newAxisVector.X / newAxisVector.Z) * 180 / Math.PI + (newAxisVector.Z > 0 ? 0 : 180);
            var perpendicularY = Vector3D.CrossProduct(new Vector3D(newAxisVector.X, 0, newAxisVector.Z), newAxisVector);
            // y方向の回転角
            var phi = Math.Asin(Math.Abs(newAxisVector.Y) / newAxisVector.Length) * 180 / Math.PI;
            var centerPoint = getCenterPoint(p0, p1);

            var tgroup = model3D.Transform as Transform3DGroup;
            if (tgroup == null)
            {
                tgroup = new Transform3DGroup();
                // 長さの変更
                tgroup.Children.Add(new ScaleTransform3D(xScale, yScale, newAxisVector.Length));
                // 角度の変更
                tgroup.Children.Add(new RotateTransform3D(new AxisAngleRotation3D(yVector, theta)));
                tgroup.Children.Add(new RotateTransform3D(new AxisAngleRotation3D(perpendicularY, phi)));
                // 中心の移動
                tgroup.Children.Add(new TranslateTransform3D(centerPoint.X, centerPoint.Y, centerPoint.Z));
            }
            else
            {
                var tgroupLength = tgroup.Children.Count;
                if (tgroupLength < 4)
                {
                    tgroup.Children.Add(new ScaleTransform3D(xScale, yScale, newAxisVector.Length));
                    tgroup.Children.Add(new RotateTransform3D(new AxisAngleRotation3D(yVector, theta)));
                    tgroup.Children.Add(new RotateTransform3D(new AxisAngleRotation3D(perpendicularY, phi)));
                    tgroup.Children.Add(new TranslateTransform3D(centerPoint.X, centerPoint.Y, centerPoint.Z));
                }
                else
                {
                    // 長さの変更
                    var st3D = tgroup.Children[tgroupLength - 4] as ScaleTransform3D;
                    if (st3D == null) throw new Exception("Couldn't able to move model");
                    st3D.ScaleX = xScale; st3D.ScaleY = yScale; st3D.ScaleZ = newAxisVector.Length;
                    tgroup.Children[tgroupLength - 4] = st3D;
                    // 角度の変更
                    var rt3D = tgroup.Children[tgroupLength - 3] as RotateTransform3D;
                    if (rt3D == null) throw new Exception("Couldn't able to move model");
                    rt3D.Rotation = new AxisAngleRotation3D(yVector, theta);
                    tgroup.Children[tgroupLength - 3] = rt3D;
                    rt3D = tgroup.Children[tgroupLength - 2] as RotateTransform3D;
                    if (rt3D == null) throw new Exception("Couldn't able to move model");
                    rt3D.Rotation = new AxisAngleRotation3D(perpendicularY, phi);
                    tgroup.Children[tgroupLength - 2] = rt3D;
                    // 中心の移動
                    var tt3D = tgroup.Children[tgroupLength - 1] as TranslateTransform3D;
                    if (tt3D == null) throw new Exception("Couldn't able to move model");
                    tt3D.OffsetX = centerPoint.X; tt3D.OffsetY = centerPoint.Y; tt3D.OffsetZ = centerPoint.Z;
                    tgroup.Children[tgroupLength - 1] = tt3D;
                }
            }
            model3D.Transform = tgroup;
            return model3D;
        }

        // p0 -> p1 -> p2 -> p3 に対し、右ねじの方向に四角形を表示する
        public static Model3DGroup CreatePlane(Point3D p0, Point3D p1, Point3D p2, Point3D p3, SolidColorBrush brush = null)
        {
            var plane = new Model3DGroup();
            plane.Children.Add(CreateTriangleModel(p0, p1, p3, brush));
            plane.Children.Add(CreateTriangleModel(p2, p3, p1, brush));
            return plane;
        }

        // 右ねじの方向が表、裏には何も表示されない
        public static Model3DGroup CreateTriangleModel(Point3D p0, Point3D p1, Point3D p2, SolidColorBrush brush = null)
        {
            var pointsList = new List<Point3D>() { p0, p1, p2 };
            var triangleMesh = new MeshGeometry3D();
            // Set each edge positions of triangle
            triangleMesh.Positions = new Point3DCollection(pointsList);
            var normal = CalculateNormals(p0, p1, p2);
            triangleMesh.Normals = new Vector3DCollection(new List<Vector3D>() { normal, normal, normal });
            var material = new DiffuseMaterial(brush != null ? brush : Brushes.White);
            var model = new GeometryModel3D(triangleMesh, material);
            var model3Dgroup = new Model3DGroup();
            model3Dgroup.Children.Add(model);
            return model3Dgroup;
        }

        public static Vector3D CalculateNormals(Point3D p0, Point3D p1, Point3D p2)
        {
            // 外積の計算
            return Vector3D.CrossProduct(p1 - p0, p2 - p1);
        }

        public void CreateCompoundImage(string filePath)
        {
            if (sphereList == null || cylinderList == null) return;
            var rtb = new RenderTargetBitmap((int)compoundViewport3D.ActualWidth, (int)compoundViewport3D.ActualHeight, 96, 96, PixelFormats.Pbgra32);
            rtb.Render(compoundViewport3D);
            var pngEncoder = new PngBitmapEncoder();
            pngEncoder.Frames.Add(BitmapFrame.Create(rtb));
            using (var fs = new FileStream(filePath, FileMode.Create, FileAccess.Write))
            {
                pngEncoder.Save(fs);
            }
        }

        #region マウス操作

        // ズームを行う
        private void control_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta < 0)
                CameraWork3D.Zoom(this.compoundViewport3D.Camera as PerspectiveCamera, Direction.Forward);
            else
                CameraWork3D.Zoom(this.compoundViewport3D.Camera as PerspectiveCamera, Direction.Backward);
        }

        private void control_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            dragStartPoint = e.GetPosition(compoundViewport3D);
        }

        private void control_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragStartPoint != null)
            {
                var mouseMovedPoint = e.GetPosition(compoundViewport3D);
                var deltaX = mouseMovedPoint.X - dragStartPoint.Value.X;
                var deltaY = mouseMovedPoint.Y - dragStartPoint.Value.Y;
                CameraWork3D.Rotate(this.compoundViewport3D.Camera as PerspectiveCamera, new Point(deltaX, deltaY), directionalLight);
                dragStartPoint = mouseMovedPoint;
            }
        }

        private void control_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            dragStartPoint = null;
        }

        #endregion

        */
    }
}
